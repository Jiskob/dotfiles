  -- Base
-- import XMonad
import XMonad hiding ( (|||) )
import XMonad.StackSet hiding (workspaces)
import System.IO
import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import qualified Codec.Binary.UTF8.String as UTF8

  -- Actions
import qualified XMonad.Actions.Search as S
import qualified XMonad.Actions.TreeSelect as TS
import XMonad.Actions.SpawnOn

  -- Hooks
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen)
import XMonad.Hooks.ManageDocks (avoidStruts, docks, ToggleStruts (..))
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.WindowSwallowing
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
-- import XMonad.Hooks.RefocusLast (refocusLastLogHook)
import XMonad.Hooks.Rescreen

  -- Data
import Data.Tree
import Data.Monoid
import Data.Char (isSpace, toUpper)

  -- System
import System.Exit
import System.Directory

  -- Util
import XMonad.Util.SpawnOnce
import XMonad.Util.Run (runProcessWithInput, spawnPipe)
import XMonad.Util.EZConfig (additionalKeysP, additionalMouseBindings)
import XMonad.Util.NamedScratchpad
-- import qualified XMonad.Util.Hacks as Hacks (trayerAboveXmobarEventHook, trayAbovePanelEventHook, trayerPaddingXmobarEventHook, trayPaddingXmobarEventHook, trayPaddingEventHook)
import XMonad.Util.Hacks (windowedFullscreenFixEventHook, trayerAboveXmobarEventHook, trayAbovePanelEventHook, trayerPaddingXmobarEventHook, trayPaddingXmobarEventHook, trayPaddingEventHook)

  -- Layout
import XMonad.Layout hiding ( (|||) )
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Simplest
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.ResizableTile
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.Named
import XMonad.Layout.Renamed
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders ( noBorders, smartBorders)
import XMonad.Layout.BoringWindows as BW (focusUp, focusDown, focusMaster, boringWindows)
import XMonad.Layout.MultiToggle (mkToggle, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers (NBFULL, NOBORDERS))
import qualified XMonad.Layout.MultiToggle as MT (Toggle (..))

  -- Prompt
import XMonad.Prompt
import XMonad.Prompt.Shell
import XMonad.Prompt.Input
import XMonad.Prompt.FuzzyMatch
import Control.Arrow (first)

------------------------------------------------------------------------
-- VARIABLES
------------------------------------------------------------------------

myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "firefox"

my2ndBrowser :: String
my2ndBrowser = "qutebrowser"

myEditor :: String
myEditor = "alacritty -e nvim"

mySpacing :: Int
mySpacing = 3

myFont :: String
-- myFont = "xft:SourceCodePro:regular:size=9:antialias=true:hinting=true"
myFont = "xft:monospace:regular:size=9:antialias=true:hinting=true"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window.
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use.
myModMask       = mod4Mask

-- The default number of workspaces (virtual screens) and their names.
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces    = ["1","2","3","4","5","6","7","8","9","0"]

------------------------------------------------------------------------
-- COLOURS
------------------------------------------------------------------------
  
-- Start flavours
base00, base01, base02, base03, base04, base05, base06, base07, base08, base09, base0A, base0B, base0C, base0D, base0E, base0F :: String
base00 = "#0c091a"
base01 = "#0c091a"
base02 = "#323232"
base03 = "#4d5b86"
base04 = "#e6e6d1"
base05 = "#f8f8f2"
base06 = "#e6e6e6"
base07 = "#f8f8f2"
base08 = "#ff5555"
base09 = "#bd93f9"
base0A = "#f1fa8c"
base0B = "#50fa7b"
base0C = "#9aedfe"
base0D = "#59dffc"
base0E = "#caa9fa"
base0F = "#bfbfbf"
-- End flavours

-- basebg = "#0c091a"
-- basefg = "#f8f8f2"

-- -- normal
-- baseblack   = "#000000"
-- basered     = "#ff5555"
-- basegreen   = "#50fa7b"
-- baseyellow  = "#f1fa8c"
-- baseblue    = "#bd93f9"
-- basemagenta = "#ff79c6"
-- basecyan    = "#8be9fd"
-- basewhite   = "#bfbfbf"

-- -- bright
-- basebrightblack   = "#323232"
-- basebrightred     = "#ff6e67"
-- basebrightgreen   = "#5af78e"
-- basebrightyellow  = "#f4f99d"
-- basebrightblue    = "#caa9fa"
-- basebrightmagenta = "#ff92d0"
-- basebrightcyan    = "#9aedfe"
-- basebrightwhite   = "#e6e6e6"

-- -- dim
-- basedimblack   = "#14151b"
-- basedimred     = "#ff2222"
-- basedimgreen   = "#1ef956"
-- basedimyellow  = "#ebf85b"
-- basedimblue    = "#4d5b86"
-- basedimmagenta = "#ff46b0"
-- basedimcyan    = "#59dffc"
-- basedimwhite   = "#e6e6d1"

------------------------------------------------------------------------
-- KEYBINDINGS
------------------------------------------------------------------------

myKeys =
    [ ("M-S-o", spawn "sh ~/scripts/shell/dmenu-kblayout/dmenu-kblayout.sh") 

    -- GridSelect
    , ("M-S-<Tab>", treeselectAction tsDefaultConfig)
    
    -- XPrompt
    , ("M-C-c", calcPrompt myXPConfig' "qalc") -- calcPrompt
    
    -- Screenshot
    , ("M-S-s", spawn "deepin-screenshot -s ~/pictures/screenshots/")
    , ("<Print>", spawn "deepin-screenshot -s ~/pictures/screenshots/")

    -- Laptop Keys
    -- Volume
    , ("<XF86AudioPlay>", spawn "playerctl --ignore-player=firefox play-pause")
    , ("<XF86AudioPrev>", spawn "playerctl --ignore-player=firefox previous")
    , ("<XF86AudioNext>", spawn "playerctl --ignore-player=firefox next")
    , ("<XF86AudioRaiseVolume>", spawn "amixer sset Master 1%+")
    , ("<XF86AudioLowerVolume>", spawn "amixer sset Master 1%-")
    , ("<XF86AudioMute>", spawn "amixer sset Master toggle")

    -- Backlight
    , ("<XF86MonBrightnessUp>", spawn "xbacklight -time 0 + 5")
    , ("<XF86MonBrightnessDown>", spawn "xbacklight -time 0 - 5")
    
    -- dunst
    , ("M-n p", spawn "dunstctl set-paused true") 
    , ("M-n s", spawn "dunstctl set-paused false") 
    , ("M-n t", spawn "dunstctl set-paused toggle") 
    , ("M-M1-<Space>", spawn "dunstctl set-paused toggle") 

    -- Applications
    , ("M-S-p", spawn "setxkbmap -option caps:escape -layout us && physlock -s")
    -- , ("M-a t", spawn (myTerminal ++ " -e nvim"))
    , ("M-a v", spawn (myTerminal ++ " -e bash $HOME/.config/vifm/scripts/vifmrun"))
    , ("M-a b", spawn (myBrowser ++ " -P"))
    , ("M-a q", spawn "qutebrowser")
    , ("M-a s", spawn "signal-desktop")
    , ("M-<Return>", spawn myTerminal)
    , ("M-a m", spawn myEditor)
    , ("M-d", spawn "dmenu_run")

    -- Scratchpads
    , ("M-C-<Return>", namedScratchpadAction myScratchPads "terminal")
    , ("M-C-p", namedScratchpadAction myScratchPads "patchmatrix")
    , ("M-C-x", namedScratchpadAction myScratchPads "keepassxc")
    , ("M-C-v", namedScratchpadAction myScratchPads "mixer")

    -- Layout Navigation
    , ("M-<Space>", sendMessage NextLayout)

    -- Jump to Layouts
    , ("M-f", setLayoutFull)
    , ("M-b", sendMessage $ ToggleStruts)
    , ("M-S-<Space> f", sendMessage $ JumpToLayout "full")
    , ("M-S-<Space> v", sendMessage $ JumpToLayout "vtall")
    , ("M-S-<Space> c", sendMessage $ JumpToLayout "htall")
    , ("M-S-<Space> <Space>", sendMessage $ JumpToLayout "floats")

    -- Sublayout Keybindings
    , ("M-C-h", sendMessage $ pullGroup L)
    , ("M-C-l", sendMessage $ pullGroup R)
    , ("M-C-k", sendMessage $ pullGroup U)
    , ("M-C-j", sendMessage $ pullGroup D)

    , ("M-C-m", withFocused (sendMessage . MergeAll))
    , ("M-C-u", withFocused (sendMessage . UnMerge))

    , ("M-C-.", onGroup W.focusUp')
    , ("M-C-,", onGroup W.focusDown')

    -- Window Navigation
    , ("M-<Tab> j", windows W.focusDown)
    , ("M-j", BW.focusDown)
    , ("M-<Tab> k", windows W.focusUp)
    , ("M-k", BW.focusUp)
    , ("M-m", BW.focusMaster)
    , ("M-S-m", windows W.swapMaster)
    , ("M-S-j", windows W.swapDown)
    , ("M-S-k", windows W.swapUp)
    
    -- Window resizing
    , ("M-M1-h", sendMessage Shrink)
    , ("M-M1-l", sendMessage Expand)
    , ("M-M1-j", sendMessage MirrorShrink)
    , ("M-M1-k", sendMessage MirrorExpand)

    , ("M-1", (windows $ W.greedyView $ myWorkspaces !! 0))
    , ("M-2", (windows $ W.greedyView $ myWorkspaces !! 1))
    , ("M-3", (windows $ W.greedyView $ myWorkspaces !! 2))
    , ("M-4", (windows $ W.greedyView $ myWorkspaces !! 3))
    , ("M-5", (windows $ W.greedyView $ myWorkspaces !! 4))
    , ("M-6", (windows $ W.greedyView $ myWorkspaces !! 5))
    , ("M-7", (windows $ W.greedyView $ myWorkspaces !! 6))
    , ("M-8", (windows $ W.greedyView $ myWorkspaces !! 7))
    , ("M-9", (windows $ W.greedyView $ myWorkspaces !! 8))
    , ("M-0", (windows $ W.greedyView $ myWorkspaces !! 9))

    -- subKeys "Send window to workspace"
    , ("M-S-1", (windows $ W.shift $ myWorkspaces !! 0))
    , ("M-S-2", (windows $ W.shift $ myWorkspaces !! 1))
    , ("M-S-3", (windows $ W.shift $ myWorkspaces !! 2))
    , ("M-S-4", (windows $ W.shift $ myWorkspaces !! 3))
    , ("M-S-5", (windows $ W.shift $ myWorkspaces !! 4))
    , ("M-S-6", (windows $ W.shift $ myWorkspaces !! 5))
    , ("M-S-7", (windows $ W.shift $ myWorkspaces !! 6))
    , ("M-S-8", (windows $ W.shift $ myWorkspaces !! 7))
    , ("M-S-9", (windows $ W.shift $ myWorkspaces !! 8))
    , ("M-S-0", (windows $ W.shift $ myWorkspaces !! 9))

    -- Push window back into tiling
    , ("M-t", withFocused $ windows . W.sink)
    -- Increment the number of windows in the master area
    , ("M-,", sendMessage (IncMasterN 1))
    -- Deincrement the number of windows in the master area
    , ("M-.", sendMessage (IncMasterN (-1)))
    
    -- close focused window
    , ("M-S-q", kill)

    -- Quit xmonad
    , ("M-S-e", io (exitWith ExitSuccess))

    -- Restart xmonad
    , ("M-S-r", spawn "xmonad --recompile; xmonad --restart")
    
    -- Notification with a summary of the search engines and their aliases (useful for beginners)
    , ("M-S-<Delete>", spawn ("notify-send -i ~/.config/dunst/icons/help-keybord-shortcuts.png \"" ++ searchengines ++ "\""))
    ]
    ++
    ----
    ---- mod-[1..9], Switch to workspace N
    ---- mod-shift-[1..9], Move client to workspace N
    ----
    -- [ (otherModMasks ++ "M-" ++ key, action tag)
    --     | (tag, key)  <- zip myWorkspaces (map (\x -> "<F" ++ show x ++ ">") [0..9])
    --     , (otherModMasks, action) <- [ ("", windows . W.greedyView) -- or W.view
    --                                  , ("S-", windows . W.shift)]
    -- ]

    ----
    ---- mod-{1,2,3, Switch to physical/Xinerama screens 1, 2, or 3
    ---- mod-shift-{1,2,3}, Move client to screen 1, 2, or 3
    ----
    [ ("M1-S-"++key, screenWorkspace sc >>= flip whenJust (windows . f))
             |(key, sc) <- zip ["1", "2", "3"] [0..]
             , (f, m) <- [(W.view, 0)]
    ] 
    -- Search Prompt
    ++ [("M-s " ++ k, S.promptSearchBrowser myXPConfig' myBrowser f) | (k,f) <- searchList ]
    ++ [("M-C-s " ++ k, S.selectSearchBrowser myBrowser f) | (k,f) <- searchList ]
    ++ [("M-M1-s " ++ k, S.promptSearchBrowser myXPConfig' my2ndBrowser f) | (k,f) <- searchList ]
    ++ [("M-M1-C-s " ++ k, S.selectSearchBrowser my2ndBrowser f) | (k,f) <- searchList ]

------------------------------------------------------------------------
-- MOUSEBINDIGS
------------------------------------------------------------------------

myMouseBindings =
    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((mod4Mask, button1), (\w -> XMonad.focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((mod4Mask, button2), (\w -> XMonad.focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((mod4Mask, button3), (\w -> XMonad.focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]


------------------------------------------------------------------------
-- LAYOUTS
------------------------------------------------------------------------

--Layouts
vtall =
    renamed [Replace "vtall"]
    $ windowNavigation
    $ subLayout [] (Simplest)
    $ smartSpacing mySpacing
    $ ResizableTall 1 (3/100) (1/2) []


htall =
    renamed [Replace "htall"]
    $ subLayout [] (Simplest)
    $ Mirror vtall

full =
    renamed [Replace "full"]
    $ noBorders
    $ Full

floats =
    renamed [Replace "floats"]
    $ noBorders
    $ simplestFloat

myLayoutHook = do
    avoidStruts 
    $ smartBorders 
    -- $ T.toggleLayouts full 
    $ mkToggle (NBFULL ?? NOBORDERS ?? EOT)
    $ boringWindows 
    $ myLayout
  where
    myLayout = 
      named "vtall" vtall |||
      named "htall" htall |||
      named "full" full |||
      named "floats" floats

------------------------------------------------------------------------
-- FUNCTIONS
------------------------------------------------------------------------

setLayoutAudio :: X ()
setLayoutAudio = do
      sendMessage $ JumpToLayout "full"
      spawn "patchmatrix"
      spawn "reaper64"

setLayoutVWA :: X ()
setLayoutVWA = do
      spawnOn (myWorkspaces !! 0) "alacritty -e nvim ~/education/autodidacticism/vwa/Meine-VWA/"
      spawnOn (myWorkspaces !! 0) "alacritty -e vifm ~/education/books/anarchosyndicalism/"
      spawnOn (myWorkspaces !! 1) "alacritty -e vifm ~/education/autodidacticism/vwa/Meine-VWA/"
      spawnOn (myWorkspaces !! 1) "alacritty -e nvim ~/education/autodidacticism/vwa/Meine-VWA/references-biblatex.bib"
      spawnOn (myWorkspaces !! 2) "qutebrowser"
      spawnOn (myWorkspaces !! 3) "alacritty -e nvim ~/documents/org/vwa.org"

setLayoutFull :: X ()
setLayoutFull = do
      sendMessage $ ToggleStruts
      sendMessage $ MT.Toggle NBFULL

-- TODO --
-- pixelize = do
  -- turn #rrggbb into 0xaarrggbb
-- TODO --

------------------------------------------------------------------------
-- TREESELECT
------------------------------------------------------------------------

treeselectAction :: TS.TSConfig (X ()) -> X ()
treeselectAction a = TS.treeselectAction a
   [ Node (TS.TSNode "+ Accessories" "Accessory applications" (return ()))
       [ Node (TS.TSNode "Picom Toggle" "Compositor for window managers" (spawn "killall picom; picom")) []
       ]
   , Node (TS.TSNode "+ Layouts" "Layouts for specific applications" (return ()))
       [ Node (TS.TSNode "Audio" "Layout for my audio-setup" (setLayoutAudio)) []
       , Node (TS.TSNode "VWA" "Layout for working on my VWA" (setLayoutVWA)) []
       ]
   , Node (TS.TSNode "+ Lightctl" "Controlling my lights" (return ()))
       [ Node (TS.TSNode "red" "" (spawn "sh ~/scripts/shell/lightctl/lightctl.sh red")) []
       , Node (TS.TSNode "blue" "" (spawn "sh ~/scripts/shell/lightctl/lightctl.sh blue")) []
       , Node (TS.TSNode "green" "" (spawn "sh ~/scripts/shell/lightctl/lightctl.sh green")) []
       , Node (TS.TSNode "purple" "" (spawn "sh ~/scripts/shell/lightctl/lightctl.sh purple")) []
       , Node (TS.TSNode "cyan" "" (spawn "sh ~/scripts/shell/lightctl/lightctl.sh cyan")) []
       , Node (TS.TSNode "white" "" (spawn "sh ~/scripts/shell/lightctl/lightctl.sh white")) []
       , Node (TS.TSNode "off" "" (spawn "sh ~/scripts/shell/lightctl/lightctl.sh off")) []
       ]
   ]

tsDefaultConfig :: TS.TSConfig a
tsDefaultConfig = TS.TSConfig { TS.ts_hidechildren = True
                              , TS.ts_background   = 0xf00c091a
                              , TS.ts_font         = myFont
                              , TS.ts_node         = (0xfff8f8f2, 0xff0c091a)
                              , TS.ts_nodealt      = (0xfff8f8f2, 0xff0c091a)
                              , TS.ts_highlight    = (0xff0c091a, 0xff50fa7b)
                              , TS.ts_extra        = 0xffd0d0d0
                              , TS.ts_node_width   = 200
                              , TS.ts_node_height  = 20
                              , TS.ts_originX      = 0
                              , TS.ts_originY      = 0
                              , TS.ts_indent       = 80
                              , TS.ts_navigate     = myTreeNavigation
                              }

myTreeNavigation = M.fromList
    [ ((0, xK_Escape),   TS.cancel)
    , ((0, xK_Return),   TS.select)
    , ((0, xK_space),    TS.select)
    , ((0, xK_Up),       TS.movePrev)
    , ((0, xK_Down),     TS.moveNext)
    , ((0, xK_Left),     TS.moveParent)
    , ((0, xK_Right),    TS.moveChild)
    , ((0, xK_k),        TS.movePrev)
    , ((0, xK_j),        TS.moveNext)
    , ((0, xK_h),        TS.moveParent)
    , ((0, xK_l),        TS.moveChild)
    , ((0, xK_o),        TS.moveHistBack)
    , ((0, xK_i),        TS.moveHistForward)
    , ((controlMask, xK_a),        TS.moveTo ["+ Accessories"])
    , ((controlMask, xK_l),        TS.moveTo ["+ Layouts"])
    , ((controlMask, xK_c),        TS.moveTo ["+ Lightctl"])
    ]

------------------------------------------------------------------------
-- XPROMPT
------------------------------------------------------------------------

myXPConfig :: XPConfig
myXPConfig = def
      { font                = myFont
      , bgColor             = base00
      , fgColor             = base05
      , bgHLight            = base03
      , fgHLight            = base00
      , borderColor         = base00
      , promptBorderWidth   = 0
      , promptKeymap        = myXPKeymap
      , position            = Top
      -- , position            = CenteredAt { xpCenterY = 0.5, xpWidth = 0.5 }
      , height              = 20
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Just 100000  -- set Just 100000 for .1 sec
      , showCompletionOnTab = False
      -- , searchPredicate     = isPrefixOf
      , searchPredicate     = fuzzyMatch
      , defaultPrompter     = id $ map toUpper  -- change prompt to UPPER
      -- , defaultPrompter     = unwords . map reverse . words  -- reverse the prompt
      -- , defaultPrompter     = drop 5 .id (++ "XXXX: ")  -- drop first 5 chars of prompt and add XXXX:
      , alwaysHighlight     = True
      , maxComplRows        = Nothing      -- set to 'Just 5' for 5 rows
      }

-- The same config above minus the autocomplete feature which is annoying
-- on certain Xprompts, like the search engine prompts.
myXPConfig' :: XPConfig
myXPConfig' = myXPConfig
      { autoComplete        = Nothing
      }

myXPKeymap :: M.Map (KeyMask,KeySym) (XP ())
myXPKeymap = M.fromList $
 map (first $ (,) controlMask) -- control + <key>
  [ (xK_u, killBefore)
  , (xK_e, killAfter)
  , (xK_0, startOfLine)
  , (xK_a, endOfLine)
  , (xK_p, pasteString)
  , (xK_h, moveWord Next)
  , (xK_h, moveWord Prev)
  , (xK_Delete, killWord Next)
  , (xK_BackSpace, killWord Prev)
  , (xK_w, killWord Prev)
  , (xK_q, quit)
  , (xK_bracketleft, quit)
  ] ++
  map (first $ (,) 0)
  [ (xK_Return, setSuccess True >> setDone True)
  , (xK_KP_Enter, setSuccess True >> setDone True)
  , (xK_BackSpace, deleteString Prev)
  , (xK_Delete, deleteString Next)
  , (xK_Left, moveCursor Prev)
  , (xK_Right, moveCursor Next)
  , (xK_Home, startOfLine)
  , (xK_End, endOfLine)
  , (xK_Down, moveHistory W.focusUp')
  , (xK_Up, moveHistory W.focusDown')
  , (xK_Escape, quit)
  ]  

-- calcPrompt
calcPrompt c ans =
    inputPrompt c (trim ans) ?+ \input ->
        liftIO(runProcessWithInput "qalc" [input] "") >>= calcPrompt c
    where
        trim  = f . f
            where f = reverse . dropWhile isSpace

------------------------------------------------------------------------
-- SEARCH ENGINE
------------------------------------------------------------------------

archwiki, reddit, urban, odysee, peertube, oxford, lyrics :: S.SearchEngine

archwiki = S.searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="
reddit   = S.searchEngine "reddit" "https://www.reddit.com/search/?q="
urban    = S.searchEngine "urban" "https://www.urbandictionary.com/define.php?term="
odysee   = S.searchEngine "odysee" "https://odysee.com/$/search?q="
peertube = S.searchEngine "peertube" "https://sepiasearch.org/search?search="
oxford   = S.searchEngine "oxford" "https://www.oxfordlearnersdictionaries.com/definition/english/"
lyrics   = S.searchEngine "lyrics" "https://www.musixmatch.com/search/"
duden    = S.searchEngine "duden" "https://www.duden.de/suchen/dudenonline/"
ordbog   = S.searchEngine "ordbog" "https://ordnet.dk/ddo/ordbog?query="
searx    = S.searchEngine "searx" "https://searx.tiekoetter.com/search?q="

-- This is the list of search engines that I want to use. Some are from
-- XMonad.Actions.Search, and some are the ones that I added above.
searchList :: [(String, S.SearchEngine)]
searchList = [ ("a", archwiki)
             , ("d", S.duckduckgo)
             , ("h", S.hoogle)
             , ("i", S.images)
             , ("r", reddit)
             , ("s", searx)
             , ("t", S.thesaurus)
             , ("v", S.vocabulary)
             , ("b", S.wayback)
             , ("u", urban)
             , ("w", S.wikipedia)
             , ("y", S.youtube)
             , ("o", odysee)
             , ("p", peertube)
             , ("z", S.amazon)
             , ("x", oxford)
             , ("n", duden)
             , ("g", ordbog)
             , ("l", lyrics)
             ]

------------------------------------------------------------------------
-- SCRATCHPADS
-----------------------------------------------------------------------

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "patchmatrix" spawnPatch findPatch managePatch
                , NS "keepassxc" spawnKee findKee manageKee
                , NS "mixer" spawnMix findMix manageMix
                ]
  where
    spawnTerm   = myTerminal ++ " -t scratchpad"
    findTerm    = title =? "scratchpad"
    manageTerm  = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnPatch  = "patchmatrix"
    findPatch   = title =? "PatchMatrix"
    managePatch = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnKee  = "keepassxc"
    findKee   = className =? "KeePassXC"
    manageKee = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnMix  = myTerminal ++ " -t term-mixer -e pulsemixer"
    findMix   = title =? "term-mixer"
    manageMix = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w

------------------------------------------------------------------------
-- MANAGEHOOK
------------------------------------------------------------------------

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "confirm"                  --> doFloat
    , className =? "file_progress"            --> doFloat
    , className =? "dialog"                   --> doFloat
    , className =? "download"                 --> doFloat
    , className =? "error"                    --> doFloat
    , className =? "Xmessage"                 --> doFloat
    , className =? "piano v2.exe"             --> doFloat
    , className =? "Carla2-Plugin"            --> doFloat
    , className =? "carla-bridge-win64.exe"   --> doFloat
    , title =? "Piano V2 (GUI)"               --> doFloat
    , title =? "menu"                         --> doFloat
    ] <+> namedScratchpadManageHook myScratchPads

------------------------------------------------------------------------
-- LOGHOOK
------------------------------------------------------------------------

-- myLogHook :: X ()
-- myLogHook = refocusLastLogHook

------------------------------------------------------------------------
-- STARTUPHOOK
------------------------------------------------------------------------

-- Perform an arbitrary action each time xmonad starts or is restarted

myStartupHook :: X ()
myStartupHook = do
      spawnOnce "setxkbmap -option caps:escape -layout us &"
      spawnOnce "autorandr -c &"
      spawnOnce "picom &"
      -- spawnOnce "bash ~/Scripts/shell/randomwallpaper &"
      spawnOnce "xwallpaper --stretch ~/pictures/warming-stripes-purple.png &"
      -- spawnOnce "/usr/bin/emacs --daemon &"
      spawn "killall trayer &"
      spawn "trayer --edge top --align right --SetDockType true --SetPartialStrut true --width 4 --expand true --transparent true --alpha 0 --tint 0x0c091a --height 20 &"
      -- spawnOnce "eww daemon &"
      -- spawnOnce "eww open wikipedia &"
      -- spawnOnce "eww open agenda &"
      spawnOnce "bash ~/scripts/shell/audio-setup &"
      spawnOnce "dunst &"
      spawnOnce "fcitx5 -d &"
      spawnOnce "xss-lock physlock &"
      -- spawnOnOnce (myWorkspaces !! 9) "alacritty -e nvim ~/documents/org/agenda.org"

------------------------------------------------------------------------
-- RESCREEN
------------------------------------------------------------------------

-- myAfterRescreenHook :: X ()
-- myAfterRescreenHook = do
--       killAllStatusBars
--       statusBarPipe dynamicSBs barSpawner

myRandrChangeHook :: X ()
-- myRandrChangeHook = spawn "autorandr -c && xrandr | grep 'HDMI-0 connected' && xrandr --output HDMI-0 --scale 1.3x1.3"
myRandrChangeHook = spawn "autorandr -c"

rescreenCfg = def {
      -- afterRescreenHook = myAfterRescreenHook,
      randrChangeHook   = myRandrChangeHook
}

------------------------------------------------------------------------
-- XMOBAR CONFIG
------------------------------------------------------------------------

myXmobarPP :: PP
myXmobarPP = filterOutWsPP [scratchpadWorkspaceTag] $ def
            { ppCurrent = xmobarColor base0B ""
            , ppVisible = xmobarColor base0F ""
            , ppHidden = wrap "" ""
            , ppUrgent = xmobarColor base08 ""
            -- , ppHiddenNoWindows = xmobarColor "#82AAFF" ""
            , ppTitle = const ""
            , ppSep =  " | "
            , ppWsSep = " "
            }

------------------------------------------------------------------------
-- XMOBAR
------------------------------------------------------------------------

xmobar0, xmobar1 :: StatusBarConfig
xmobar0   = statusBarPropTo "_XMONAD_LOG_0" "xmobar -x 0 ~/.config/xmobar/xmobar0.hs" (pure myXmobarPP)
xmobar1   = statusBarPropTo "_XMONAD_LOG_1" "xmobar -x 1 ~/.config/xmobar/xmobar1.hs" (pure myXmobarPP)

barSpawner :: ScreenId -> IO StatusBarConfig
barSpawner 0 = pure xmobar0
barSpawner 1 = pure xmobar1
barSpawner _ = mempty -- nothing on the rest of the screens

------------------------------------------------------------------------
-- MAIN CONFIG
------------------------------------------------------------------------

myConfig = def
    { 
    -- simple stuff
    terminal             = myTerminal
    , focusFollowsMouse  = myFocusFollowsMouse
    , clickJustFocuses   = myClickJustFocuses
    , borderWidth        = myBorderWidth
    , modMask            = myModMask
    , workspaces         = myWorkspaces
    , normalBorderColor  = base00
    , focusedBorderColor = base0B

    -- hooks, layouts
    , layoutHook         = myLayoutHook
    , manageHook         = manageSpawn <+> myManageHook
    , handleEventHook    = swallowEventHook (className =? "Alacritty") (return True) <+> windowedFullscreenFixEventHook <> trayerPaddingXmobarEventHook
    , startupHook        = myStartupHook
    -- , logHook            = myLogHook
    }
    `additionalKeysP` myKeys
    `additionalMouseBindings` myMouseBindings

------------------------------------------------------------------------
-- MAIN
------------------------------------------------------------------------

main :: IO()
main = xmonad
     . docks
     . ewmh
     . ewmhFullscreen
     . withUrgencyHook NoUrgencyHook
     . rescreenHook rescreenCfg
     . dynamicSBs barSpawner
     $ myConfig

------------------------------------------------------------------------
-- SEARCH ENGINE HELP
------------------------------------------------------------------------

searchengines :: String
searchengines = unlines ["Search engines and their aliases: ",
    "",
    " <super> + <s>",
    "   + <a>    ->    archwiki",
    "   + <b>    ->    wayback",
    "   + <d>    ->    duckduckgo",
    "   + <h>    ->    hoogle",
    "   + <i>    ->    images",
    "   + <o>    ->    odysee",
    "   + <p>    ->    peertube",
    "   + <r>    ->    reddit",
    "   + <s>    ->    searx",
    "   + <t>    ->    thesaus",
    "   + <u>    ->    urban",
    "   + <v>    ->    vocabulary",
    "   + <w>    ->    wikipedia",
    "   + <x>    ->    oxford",
    "   + <n>    ->    duden",
    "   + <g>    ->    ordbog",
    "   + <y>    ->    youtube",
    "   + <z>    ->    amazon",
    "   + <l>    ->    lyrics"]
