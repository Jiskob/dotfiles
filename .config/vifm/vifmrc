"        _  __
" __   _(_)/ _|_ __ ___  _ __ ___
" \ \ / / | |_| '_ ` _ \| '__/ __|
"  \ V /| |  _| | | | | | | | (__
"   \_/ |_|_| |_| |_| |_|_|  \___|
"

set vicmd=nvim

" This makes vifm perform file operations on its own instead of relying on
" standard utilities like `cp`.  While using `cp` and alike is a more universal
" solution, it's also much slower when processing large amounts of files and
" doesn't support progress measuring.

set syscalls

" Trash Directory
" The default is to move files that are deleted with dd or :d to
" the trash directory.  If you change this you will not be able to move
" files by deleting them and then using p to put the file in the new location.
" I recommend not changing this until you are familiar with vifm.
" This probably shouldn't be an option.

set trash

" This is how many directories to store in the directory history.

set history=100

" Automatically resolve symbolic links on l or Enter.

set nofollowlinks

" With this option turned on you can run partially entered commands with
" unambiguous beginning using :! (e.g. :!Te instead of :!Terminal or :!Te<tab>).

" set fastrun

" Natural sort of (version) numbers within text.

set sortnumbers

" Maximum number of changes that can be undone.

set undolevels=100

" If you installed the vim.txt help file set vimhelp.
" If would rather use a plain text help file set novimhelp.

set novimhelp

" If you would like to run an executable file when you
" press return on the file name set this.

set norunexec

" Selected color scheme
" The following line will cause issues if using vifm.vim with regular vim.
" Either use neovim or comment out the following line.
colorscheme default

" Format for displaying time in file list. For example:
" TIME_STAMP_FORMAT=%m/%d-%H:%M
" See man date or man strftime for details.

set timefmt=%m/%d\ %H:%M

" Show list of matches on tab completion in command-line mode

set wildmenu

" Display completions in a form of popup with descriptions of the matches

set wildstyle=popup

" Display suggestions in normal, visual and view modes for keys, marks and
" registers (at most 5 files).  In other view, when available.

set suggestoptions=normal,visual,view,otherpane,keys,marks,registers

" Ignore case in search patterns unless it contains at least one uppercase
" letter

set ignorecase
set smartcase

" Don't highlight search results automatically

set nohlsearch

" Use increment searching (search while typing)
set incsearch

" Try to leave some space from cursor to upper/lower border in lists

set scrolloff=4

" Don't do too many requests to slow file systems

if !has('win')
    set slowfs=curlftpfs
endif

" Set custom status line look

set statusline="  Hint: %z%= %A %10u:%-7g %15s %20d  "

" Set line numbers to show

" ------------------------------------------------------------------------------

" :mark mark /full/directory/path [filename]

mark h ~/

" ------------------------------------------------------------------------------

" :com[mand][!] command_name action
" The following macros can be used in a command
" %a is replaced with the user arguments.
" %c the current file under the cursor.
" %C the current file under the cursor in the other directory.
" %f the current selected file, or files.
" %F the current selected file, or files in the other directory.
" %b same as %f %F.
" %d the current directory name.
" %D the other window directory name.
" %m run the command in a menu window

command! df df -h %m 2> /dev/null
command! diff vim -d %f %F
command! zip zip -r %f.zip %f
command! run !! ./%f
command! make !!make %a
command! mkcd :mkdir %a | cd %a
command! vgrep vim "+grep %a"
command! reload :write | restart

" ------------------------------------------------------------------------------

" Pdf
filextype *.pdf zathura %c %i &, apvlv %c, xpdf %c
fileviewer *.pdf
        " \ ~/.config/vifm/scripts/vifmimg pdfpreview %px %py %pw %ph %c
        \ %pc
        \ ~/.config/vifm/scripts/vifmimg clear
        " \ pdftotext -nopgbrk %c -

" PostScript
filextype *.ps,*.eps,*.ps.gz
        \ {View in zathura}
        \ zathura %f,

" Djvu
filextype *.djvu
        \ {View in zathura}
        \ zathura %f,

" Epub
filextype *.epub
        \ {View in zathura}
        \ zathura %f,

" Audio
filetype *.wav,*.mp3,*.flac,*.m4a,*.wma,*.ape,*.ac3,*.og[agx],*.spx,*.opus
       \ {View using mpv}
       \ mpv %f,
       "\ {Play using ffplay}
       "\ ffplay -nodisp -autoexit %c,
fileviewer *.mp3 mp3info
fileviewer *.flac soxi

" Video
filextype *.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,*.vob,
         \*.fl[icv],*.m2v,*.mov,*.webm,*.ts,*.mts,*.m4v,*.r[am],*.qt,*.divx,
         \*.as[fx]
        \ {View using mpv}
        \ mpv %f,
fileviewer *.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,*.vob,
          \*.fl[icv],*.m2v,*.mov,*.webm,*.ts,*.mts,*.m4v,*.r[am],*.qt,*.divx,
          \*.as[fx]
        \ ~/.config/vifm/scripts/vifmimg videopreview %px %py %pw %ph %c 
        \ %pc 
        \ ~/.config/vifm/scripts/vifmimg clear
        " \ ffprobe -pretty %c 2>&1

" Web
filextype *.html,*.htm
        \ {Open with vim}
        \ vim %c &,
        \ {Open with librewolf}
        \ librewolf %f &,
filetype *.html,*.htm links, lynx

" Object
filetype *.o nm %f | less

" Man page
filetype *.[1-8] man ./%c
fileviewer *.[1-8] man ./%c | col -b

" Images
filextype *.bmp,*.jpg,*.jpeg,*.png,*.gif,*.xpm,*.webp
        \ {View in sxiv}
        \ sxiv -ia %f &,
fileviewer *.bmp,*.jpg,*.jpeg,*.png,*.xpm,*webp
        \ ~/.config/vifm/scripts/vifmimg draw %px %py %pw %ph %c
        \ %pc
        \ ~/.config/vifm/scripts/vifmimg clear
fileviewer *.gif
        \ ~/.config/vifm/scripts/vifmimg gifpreview %px %py %pw %ph %c
        \ %pc
        \ ~/.config/vifm/scripts/vifmimg clear
fileviewer *.ico
        \ ~/.config/vifm/scripts/vifmimg magickpreview %px %py %pw %ph %c
        \ %pc
        \ ~/.config/vifm/scripts/vifmimg clear

" Office files
filextype *.odt,*.doc,*.docx,*.xls,*.xlsx,*.odp,*.pptx libreoffice %f &

" Directories
filextype */
        \ {View in thunar}
        \ Thunar %f &,

" file types
set classify=' :dir:/, :exe:, :reg:, :link:'
" various file names
set classify+='﬋ ::../::, ::*.sh::, ::*.[hc]pp::, ::*.[hc]::, ::/^copying|license$/::, ::.git/,,*.git/::, ::*.epub,,*.fb2,,*.djvu::, ::*.pdf::, ::*.htm,,*.html,,**.[sx]html,,*.xml::, ::*.py,,*.pyi,,*.pyc,,*pyd,,*.pyo,,*.pyw,,*.pyz::, ::*.rs,,*.rlib::'
" archives
set classify+=' ::*.7z,,*.ace,,*.arj,,*.bz2,,*.cpio,,*.deb,,*.dz,,*.gz,,*.jar,,*.lzh,,*.lzma,,*.rar,,*.rpm,,*.rz,,*.tar,,*.taz,,*.tb2,,*.tbz,,*.tbz2,,*.tgz,,*.tlz,,*.trz,,*.txz,,*.tz,,*.tz2,,*.xz,,*.z,,*.zip,,*.zoo::'
" images
set classify+=' ::*.bmp,,*.gif,,*.jpeg,,*.jpg,,*.ico,,*.png,,*.ppm,,*.svg,,*.svgz,,*.tga,,*.tif,,*.tiff,,*.xbm,,*.xcf,,*.xpm,,*.xspf,,*.xwd::'
" audio
set classify+=' ::*.aac,,*.anx,,*.asf,,*.au,,*.axa,,*.flac,,*.m2a,,*.m4a,,*.mid,,*.midi,,*.mp3,,*.mpc,,*.oga,,*.ogg,,*.ogx,,*.ra,,*.ram,,*.rm,,*.spx,,*.wav,,*.wma,,*.ac3::'
" media
set classify+=' ::*.avi,,*.ts,,*.axv,,*.divx,,*.m2v,,*.m4p,,*.m4v,,.mka,,*.mkv,,*.mov,,*.mp4,,*.flv,,*.mp4v,,*.mpeg,,*.mpg,,*.nuv,,*.ogv,,*.pbm,,*.pgm,,*.qt,,*.vob,,*.wmv,,*.xvid::'
" office files
set classify+=' ::*.doc,,*.docx::, ::*.xls,,*.xls[mx]::, ::*.pptx,,*.ppt::'

" Syntax highlighting in preview
"
" Explicitly set highlight type for some extensions
"
" 256-color terminal
" fileviewer *.[ch],*.[ch]pp highlight -O xterm256 -s dante --syntax c %c
" fileviewer Makefile,Makefile.* highlight -O xterm256 -s dante --syntax make %c
"
" 16-color terminal
" fileviewer *.c,*.h highlight -O ansi -s dante %c
"
" Or leave it for automatic detection
"
" fileviewer *[^/] pygmentize -O style=monokai -f console256 -g

" Displaying pictures in terminal
"
" fileviewer *.jpg,*.png shellpic %c

" Open all other files with default system programs (you can also remove all
" :file[x]type commands above to ensure they don't interfere with system-wide
" settings).  By default all unknown files are opened with 'vi[x]cmd'
" uncommenting one of lines below will result in ignoring 'vi[x]cmd' option
" for unknown file types.
" For *nix:
" filetype * xdg-open
" For OS X:
" filetype * open
" For Windows:
" filetype * start, explorer

" ------------------------------------------------------------------------------

" What should be saved automatically between vifm runs
" Like in previous versions of vifm
" set vifminfo=options,filetypes,commands,bookmarks,dhistory,state,cs
" Like in vi
set vifminfo=dhistory,savedirs,chistory,state,tui,shistory,
    \phistory,fhistory,dirstack,registers,bookmarks,bmarks

" ------------------------------------------------------------------------------

" Examples of configuring both panels

" Customize view columns a bit (enable ellipsis for truncated file names)
"
" set viewcolumns=-{name}..,6{}.

" Filter-out build and temporary files
"
" filter! /^.*\.(lo|o|d|class|py[co])$|.*~$/

" ------------------------------------------------------------------------------

" Sample mappings

"Open all images in current directory in sxiv thumbnail mode
nnoremap sx :!sxiv -t %d & <cr>

"Open selected images in gimp
nnoremap gp :!gimp %f & <cr>

" Start shell in current directory
nnoremap s :shell<cr>

" Display sorting dialog
nnoremap S :sort<cr>

" Toggle visibility of preview window
nnoremap w :view<cr>
vnoremap w :view<cr>gv

" Open file in the background using its default program
nnoremap gb :file &<cr>l

" Yank current directory path into the clipboard
nnoremap yd :!echo %d | xclip %i<cr>

" Yank current file path into the clipboard
nnoremap yf :!echo %c:p | xclip %i<cr>

" Mappings for faster renaming
nnoremap I cw<c-a>
nnoremap cc cw<c-u>
nnoremap A cw

" Open console in current directory
nnoremap ,t :!alacritty &<cr>

" Open editor to edit vifmrc and apply settings after returning to vifm
nnoremap ,c :write | edit $MYVIFMRC | restart<cr>
" Open gvim to edit vifmrc
nnoremap ,C :!nvim --remote-tab-silent $MYVIFMRC &<cr>

" Toggle wrap setting on ,w key
nnoremap ,w :set wrap!<cr>

" Example of standard two-panel file managers mappings
nnoremap <f3> :!less %f<cr>
nnoremap <f4> :edit<cr>
nnoremap <f5> :copy<cr>
nnoremap <f6> :move<cr>
nnoremap <f7> :mkdir<space>
nnoremap <f8> :delete<cr>

" ------------------------------------------------------------------------------

" Various customization examples

" Use ag (the silver searcher) instead of grep
"
" set grepprg='ag --line-numbers %i %a %s'

" Add additional place to look for executables
"
" let $PATH = $HOME.'/bin/fuse:'.$PATH

" Block particular shortcut
"
" nnoremap <left> <nop>

" Export IPC name of current instance as environment variable and use it to
" communicate with the instance later.
"
" It can be used in some shell script that gets run from inside vifm, for
" example, like this:
"     vifm --server-name "$VIFM_SERVER_NAME" --remote +"cd '$PWD'"
"
" let $VIFM_SERVER_NAME = v:servername

map > :!~/.config/vifm/scripts/vifmimg inc<CR>
map < :!~/.config/vifm/scripts/vifmimg dec<CR>
