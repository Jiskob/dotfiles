EDITOR='nvim'
BROWSER="librewolf"

# Enable colors and change prompt:
autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

# Basic auto/tab complete:
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# Vi-Mode
bindkey -v
export KEYTIMEOUT=1

alias doas="doas -- "
alias amk="doas"
alias vim="nvim"
alias hx="helix"
alias vifm="bash $HOME/.config/vifm/scripts/vifmrun"
alias vifmimg="bash $HOME/.config/vifm/scripts/vifmimg"
alias yeet="paru -Rns"
alias orphan="paru -Qdt"
alias ls="exa --color=always --group-directories-first"
alias la="exa -a --color=always --group-directories-first"
alias df="df -h"
alias ufetch="bash ~/scripts/shell/ufetch-arch"
alias syscleaner="sh ~/scripts/shell/system-cleaner/syscleaner.sh"
alias germanize="sed 's/oe/ö/g; s/ae/ä/g; s/ue/ü/g; s/sz/ß/g'"
alias copypasta="python ~/scripts/python/banner.py"
alias mv="mv -i"
alias rm="rm -i"
alias gdots="/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME"
alias gpwall="git push git@gitlab.com:Jiskob/wallpapers.git"
alias gpdots="gdots push git@gitlab.com:Jiskob/dotfiles.git"
alias startx="startx ~/.xinitrc"
alias cs="cd ~/education/heterodidacticism/school/"
alias cu="cd ~/education/heterodidacticism/uni/"
alias transen="dialect -d en -t"
alias transch="dialect -d ch -t"
alias urmum="echo 'Error: Object exceeds disk space. Execution failed (69)'"
alias whoisurmum="sxiv -z 300 ~/Pictures/nauti-full.png"
alias ssh-tunnel-i2p="sh $HOME/Scripts/shell/ssh-tunnel-i2p/ssh-tunnel-i2p"
alias cs="cd ~/education/heterodidacticism/school/"
alias cu="cd ~/education/heterodidacticism/uni/"
alias transen="dialect -d en -t"
alias transch="dialect -d ch -t"
alias urmum="echo 'Error: Object exceeds disk space. Execution failed (69)'"
alias whoisurmum="sxiv -z 300 ~/pictures/nauti-full.png"
alias ssh-tunnel-i2p="sh $HOME/scripts/shell/ssh-tunnel-i2p/ssh-tunnel-i2p"

# extract files
function extract () {
   if [ -f $1 ] ; then
      case $1 in
         *.tar.bz2)  tar xjf $1;;
         *.tar.gz)   tar xzf $1;;
         *.tar.xz)   tar xf $1;;
         *.bz2)      bunzip2 $1;;
         *.rar)      unrar x $1;;
         *.gz)       gunzip $1;;
         *.tar)      tar xf $1;;
         *.tbz2)     tar xjf $1;;
         *.tgz)      tar xzf $1;;
         *.zip)      unzip $1;;
         *.Z)        uncompress $1;;
         *.7z)       7z x $1;;
         *.tar.zst)  tar --use-compress-program=unzstd -xvf $1;;
         *.zst)      unzstd $1;;
         *.xz)       unxz $1;;
         *)          echo "'$1' cannot be extracted via extract()" ;;
      esac
   else
      echo "'$1' is not a valid file"
   fi
} 

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# Load Starship Prompt
eval "$(starship init zsh)"
