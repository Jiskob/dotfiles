export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

export TERM='xterm-256color'
export PATH='/usr/local/sbin:/usr/local/bin:/usr/bin:/home/user1/scripts/shell/:/home/user1/.cargo/bin/:/home/user1/.local/bin:/var/lib/flatpak/exports/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl'
export MANPAGER='nvim +Man!'

export EDITOR='nvim'

export GBM_BACKEND=nvidia-drm
export __GLX_VENDOR_LIBRARY_NAME=nvidia
