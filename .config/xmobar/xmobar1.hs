-- Xmobar (http://projects.haskell.org/xmobar/)

-- Config { font            = "xft:Source Code Pro:pixelsize=12:antialias=true:hinting=true"
-- Config { font            = "xft:monospace:pixelsize=11:antialias=true:hinting=true"
--         , additionalFonts       = [
--                                 "xft:Font Awesome 6 Free Solid:pixelsize=8:antialias=true:hinting=true"
--                                 , "xft:Mononoki Nerd Font:pixelsize=8:antialias=true:hinting=true"
--                                 ]
Config { font            = "Hack 10"
        , additionalFonts       = [
                                "Font Awesome 6 Free Solid 7"
                                , "Mononoki Nerd Font 7"
                                ]
        , bgColor       = "#0c091a"
        , fgColor       = "#f8f8f2"
        , position      = TopSize L 100 22
        , lowerOnStart  = True
        , hideOnStart   = False
        , allDesktops   = True
        , persistent    = True
        , iconRoot      = "/home/user1/.config/xmobar/icons/"
        , commands =    [
                        Run Kbd [("us(dvorak)", "dv")]
                        -- Get daily covid cases
                        , Run Com "/home/user1/.config/xmobar/scripts/get-covid.sh" [] "covid" 36000
                        -- Get the song thats being played
                        , Run Mpris2 "ncspot" ["-f", "'{icon} {:t37:{artist} - {title}:}'", "-M", "16", "--nastring", ""] 10
                        , Run Mpris2 "cmus" ["-f", "'{icon} {:t37:{artist} - {title}:}'", "-M", "16", "--nastring", ""] 10
                        -- Cpu usage in percent
                        , Run MultiCpu ["-t", "<fn=1>\xf108</fn> cpu: <fc=#50fa7b,#282a36><fn=2><autovbar></fn></fc> (<total>%)","-H","50","--high","red"] 30
                        -- Ram used number and percent
                        , Run Memory ["-t", "<fn=1>\xf233</fn> mem: <used>M (<usedratio>%)"] 30
                        -- Audio
                        , Run Volume "default" "Master" ["-t", "<fn=1><status></fn> <volume>%", "--", "-O", "", "-o", "", "-C", "#50fa7b", "-c", "#ff5555", "-O",  "\xf028 ", "-o", "\xf026 "] 1
                        -- Notifications
                        , Run Com "/home/user1/scripts/shell/dunst-status/dunst-status.sh" [] "notification" 1
                        -- Time and date
                        , Run Date "<fn=1>\xf017</fn> %Y-%m-%d %H:%M:%S" "date" 1
                        -- Script that dynamically adjusts xmobar padding depending on number of trayer icons.
                        -- , Run Com ".config/xmobar/trayer-padding-icon.sh" [] "trayerpad" 30
                        -- Prints out the left side items such as workspaces, layout, etc
                        -- , Run UnsafeStdinReader
                        , Run UnsafeXPropertyLog "_XMONAD_LOG_1"
                        ]
        , sepChar = "%"
        , alignSep = "}{"
        , template = " %kbd% | %_XMONAD_LOG_1% }{<fc=#ff79c6>%mpris2%</fc> | <fc=#bd93f9>%covid%</fc> | %multicpu% | <fc=#bd93f9>%memory%</fc> | %default:Master% | <fc=#f1fa8c><fn=1>%notification%</fn></fc> | <fc=#f1fa8c>%date%</fc> "
        }
